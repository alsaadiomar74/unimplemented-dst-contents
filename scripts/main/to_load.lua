return {
	Prefabs = {
		"grotto_pillar_bug",
		"grotto_bug_house",
		"grotto_mowlth",
		"otter",
		"puffer",
		"boat_driftwood",
		"rocky_claws",
	},

	Assets = {
		Asset("ANIM", "anim/boat_plank_driftwood_build.zip"),
		Asset("ANIM", "anim/boat_leak_driftwood_build.zip"),
		Asset("ANIM", "anim/boat_brokenparts_driftwood_build.zip"),
	}
}
