local seg_time = 30
local total_day_time = seg_time*16

local day_segs = 10
local dusk_segs = 4
local night_segs = 2

--default day composition. changes in winter, etc
local day_time = seg_time * day_segs
local dusk_time = seg_time * dusk_segs
local night_time = seg_time * night_segs

local multiplayer_attack_modifier = 1--0.6--0.75
local multiplayer_goldentool_modifier = 1--0.5--0.75
local multiplayer_armor_durability_modifier = 0.7
local multiplayer_armor_absorption_modifier = 1--0.75
local multiplayer_wildlife_respawn_modifier = 1--2

local wilson_attack = 34 * multiplayer_attack_modifier
local wilson_health = 150
local wilson_hunger = 150
local wilson_sanity = 200
local calories_per_day = 75

local wilson_attack_period = 0.4 --prevents players

-----------------------

local perish_warp = 1--/200

local OCEAN_NOISE_BASE_SCALE = 10
local OCEAN_SPEED_BASE_SCALE = 0.01


------------------------------
TUNING.CLAW_GLOVE_USES = 200
TUNING.CLAW_GLOVE_DAMAGE = wilson_attack*1.5
------------------------------

TUNING.OTTER_HEALTH = 250
TUNING.OTTER_DAMAGE = 45
TUNING.OTTER_ATTACK_PERIOD = 1
TUNING.OTTER_HIT_RANGE = 2

TUNING.OTTER_WALK_SPEED = 6
TUNING.OTTER_RUN_SPEED = 7
TUNING.OTTER_RUN_WATER_SPEED = 8

TUNING.OTTER_FISH_TARGET_DIST = 1.5
TUNING.OTTER_TARGET_DIST = 4

------------------------------
TUNING.PUFFER_HEALTH = 250
TUNING.PUFFER_DAMAGE = 45
TUNING.PUFFER_ATTACK_PERIOD = 2
TUNING.PUFFER_HIT_RANGE = 3

TUNING.PUFFER_WALK_SPEED = 3
TUNING.PUFFER_RUN_SPEED = 6

TUNING.PUFFER_TARGET_DIST = 4
TUNING.PUFFER_BOAT_TARGET_DIST = 5
