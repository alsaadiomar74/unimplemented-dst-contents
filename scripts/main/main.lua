AddPrefabPostInit("walkingplank", function(inst)
    inst.SetType = function(inst, type)
        if type == "driftwood" then
            inst.AnimState:SetBuild("boat_plank_driftwood_build")
        end
    end
end)

AddPrefabPostInit("boat_leak", function(inst)
    inst.SetType = function(inst, type)
        if type == "driftwood" then
            inst.AnimState:SetBuild("boat_leak_driftwood_build")
        end
    end
end)

AddPrefabPostInit("boatlip", function(inst)
    inst.SetType = function(inst, type)
        if type == "driftwood" then
            inst.AnimState:SetBuild("boat_driftwood")
        end
    end
end)
