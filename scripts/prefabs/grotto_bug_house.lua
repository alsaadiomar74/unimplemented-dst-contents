local assets =
{
    Asset("ANIM", "anim/grotto_bug_house.zip"),
}

local prefabs =
{
    "grotto_mowlth",
}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("grotto_bug_house")
    inst.AnimState:SetBuild("grotto_bug_house")
    inst.AnimState:PlayAnimation("idle"..math.random(1,3), true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")
	
    return inst
end

return Prefab("grotto_bug_house", fn, assets, prefabs)