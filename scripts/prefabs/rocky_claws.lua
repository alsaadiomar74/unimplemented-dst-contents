local assets =
{
    Asset("ANIM", "anim/rocky_claw.zip"),
    Asset("ANIM", "anim/swap_rocky_claw.zip"),
}

local function onequip(inst, owner)
    owner.AnimState:OverrideSymbol("hand", "swap_rocky_claw", "swap_hand")
end

local function onunequip(inst, owner)
    owner.AnimState:ClearOverrideSymbol("hand")
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("rocky_claw")
    inst.AnimState:SetBuild("rocky_claw")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("sharp")
    inst:AddTag("pointy")

    --weapon (from weapon component) added to pristine state for optimization
    inst:AddTag("weapon")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
    inst.components.weapon:SetDamage(TUNING.CLAW_GLOVE_DAMAGE)

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.CLAW_GLOVE_USES)
    inst.components.finiteuses:SetUses(TUNING.CLAW_GLOVE_USES)

    inst.components.finiteuses:SetOnFinished(inst.Remove)

    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")

    inst:AddComponent("equippable")
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("rocky_claw", fn, assets)