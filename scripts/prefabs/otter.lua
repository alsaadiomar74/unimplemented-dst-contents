local brain = require "brains/otterbrain"

local assets =
{
    Asset("ANIM", "anim/otter_build.zip"),
    Asset("ANIM", "anim/otter_basic.zip"),
    Asset("ANIM", "anim/otter_basic_water.zip"),
}

local prefabs = {}

local TARGET_MUST_TAGS = { "_combat", "character" }
local TARGET_CANT_TAGS = { "otter", "INLIMBO" }
local function FindTarget(inst, radius)
    if not inst.no_targeting then
        return FindEntity(
            inst,
            SpringCombatMod(radius),
            function(guy)
                return (not guy:HasTag("monster") or guy:HasTag("player"))
                    and inst.components.combat:CanTarget(guy)
                    and not (inst.components.follower ~= nil and inst.components.follower.leader == guy)
                    and not (inst.components.follower.leader ~= nil and inst.components.follower.leader:HasTag("player") 
                        and guy:HasTag("player") and not TheNet:GetPVPEnabled())
            end,
            TARGET_MUST_TAGS,
            TARGET_CANT_TAGS
        )
    end
end

local function Retarget(inst)
    local dist = (inst._fishtarget ~= nil and inst._fishtarget:IsValid() and TUNING.OTTER_FISH_TARGET_DIST)
        or inst.components.knownlocations:GetLocation("investigate") ~= nil and TUNING.SPIDER_INVESTIGATETARGET_DIST
        or TUNING.OTTER_TARGET_DIST

    return FindTarget(inst, dist)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 100, .75)

    inst.DynamicShadow:SetSize(2.5, 1)
    inst.Transform:SetSixFaced()

    inst:AddTag("animal")
    inst:AddTag("otter")
    
    inst.AnimState:SetBank("otter_basics")
    inst.AnimState:SetBuild("otter_build")
    inst.AnimState:PlayAnimation("idle", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("eater")
    inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetStrongStomach(true) -- can eat monster meat!
    inst.components.eater:SetCanEatRawMeat(true)
    
    inst:AddComponent("timer")

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.OTTER_DAMAGE)
    inst.components.combat:SetAttackPeriod(TUNING.OTTER_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.OTTER_HIT_RANGE)
    inst.components.combat:SetRetargetFunction(2, Retarget)

    inst:AddComponent("follower")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.OTTER_HEALTH)

    inst:AddComponent("lootdropper")

    inst:AddComponent("inspectable")

    inst:AddComponent("knownlocations")

    MakeHauntablePanic(inst)

    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = TUNING.OTTER_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.OTTER_RUN_SPEED
    inst.components.locomotor:CanPathfindOnWater()

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGotter")

    inst:AddComponent("embarker")
    inst.components.embarker.embark_speed = inst.components.locomotor.runspeed

    inst.components.locomotor:SetAllowPlatformHopping(true)

    inst:AddComponent("amphibiouscreature")
    inst.components.amphibiouscreature:SetBanks("otter_basics", "otter_basics_water")
    inst.components.amphibiouscreature:SetEnterWaterFn(function(inst)
        inst.landspeed = inst.components.locomotor.runspeed
        inst.components.locomotor.runspeed = TUNING.OTTER_RUN_WATER_SPEED
        inst.hop_distance = inst.components.locomotor.hop_distance
        inst.components.locomotor.hop_distance = 4
    end)
    inst.components.amphibiouscreature:SetExitWaterFn(function(inst)
        if inst.landspeed then
            inst.components.locomotor.runspeed = TUNING.OTTER_RUN_SPEED
        end
        if inst.hop_distance then
            inst.components.locomotor.hop_distance = inst.hop_distance
        end
    end)

    inst.components.locomotor.pathcaps = { allowocean = true }

    return inst
end

return Prefab("otter", fn, assets, prefabs)
