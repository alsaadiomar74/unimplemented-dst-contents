local assets =
{
    Asset("ANIM", "anim/grotto_pillar_bug.zip"),
}

local prefabs = {}

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("grotto_pillar_bug")
    inst.AnimState:SetBuild("grotto_pillar_bug")
    inst.AnimState:PlayAnimation("idle", true)
	
	local type = math.random(1,3) 
	
	if type == 1 then
		inst.AnimState:HideSymbol("grub_goo")
	elseif type == 2 then
		inst.AnimState:OverrideSymbol("grub_goo", "grotto_pillar_bug", "grub_goo_half")
	end
	
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:AddComponent("inspectable")
	
    return inst
end

return Prefab("grotto_pillar_bug", fn, assets, prefabs)