local assets =
{
    Asset("ANIM", "anim/lavaarena_centaurpede_basic.zip"),
}

local assets =
{
    Asset("ANIM", "anim/lavaarena_centaurpede_torso_basic.zip"),
}

local prefabs =
{
	"centaurpede_torso",
}

local torso_prefabs =
{
	"centaurpede",
}

local brain = require("brains/centaurpedebrain")

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 9999, .5)

    inst.DynamicShadow:SetSize(6.0, 3.0)
    inst.Transform:SetSixFaced()

    inst:AddTag("epic")
    inst:AddTag("scarytoprey")

	inst.AnimState:SetBuild("lavaarena_centaurpede_basic")
    inst.AnimState:SetBank("centaurpede")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")

    inst:AddComponent("locomotor")
    inst.components.locomotor.runspeed = 8
    inst.components.locomotor.walkspeed = 4.6

    inst:AddComponent("eater")
    inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })

    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "chest"
    ------------------------------------------
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(2000)

    inst:AddComponent("inventory")

    inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"meat", "meat"})

    inst:AddComponent("knownlocations")
	
	inst:ListenForEvent("attacked", OnAttacked)

    MakeMediumFreezableCharacter(inst, "chest")
	MakeMediumBurnableCharacter(inst, "chest")

    inst:SetBrain(brain)
    inst:SetStateGraph("SGcentaurpede")

    return inst
end

local function torso()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 9999, .5)

    inst.DynamicShadow:SetSize(6.0, 3.0)
    inst.Transform:SetSixFaced()

    inst:AddTag("scarytoprey")

	inst.AnimState:SetBuild("lavaarena_centaurpede_torso_basic")
    inst.AnimState:SetBank("centaurpede_torso")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")

    inst:AddComponent("locomotor")
    inst.components.locomotor.runspeed = 8
    inst.components.locomotor.walkspeed = 4.6

    inst:AddComponent("eater")
    inst.components.eater:SetDiet({ FOODTYPE.MEAT }, { FOODTYPE.MEAT })

    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "bod"
    ------------------------------------------
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(100)

    inst:AddComponent("inventory")

    inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"smallmeat"})

    MakeMediumFreezableCharacter(inst, "bod")
	MakeMediumBurnableCharacter(inst, "bod")

    inst:SetStateGraph("SGcentaurpede")

    return inst
end

return Prefab("centaurpede", fn, assets, prefabs),
	Prefab("centaurpede_torso", torso, torso_assets, torso_prefabs)