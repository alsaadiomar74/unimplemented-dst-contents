local assets =
{
    Asset("ANIM", "anim/grotto_mowlth_basic.zip"),
}

local prefabs =
{
    "meat",
}

local brain = require("brains/mowlthbrain")

local function OnAttacked(inst, data)
    inst.components.combat:SetTarget(data.attacker)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 50, .5)

    inst.DynamicShadow:SetSize(6.0, 3.0)
    inst.Transform:SetFourFaced()

    inst:AddTag("cavedweller")
    inst:AddTag("scarytoprey")

	inst.AnimState:SetBuild("grotto_mowlth_basic")
    inst.AnimState:SetBank("mowlth")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")

    inst:AddComponent("locomotor")
    inst.components.locomotor.runspeed = 7
    inst.components.locomotor.walkspeed = 4

    inst:AddComponent("eater")
    inst.components.eater:SetDiet({ FOODTYPE.VEGGIE }, { FOODTYPE.VEGGIE })
    inst.components.eater:SetCanEatRaw()

    inst:AddComponent("combat")
    inst.components.combat.hiteffectsymbol = "mowlth_bod"
    ------------------------------------------
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(900)

    inst:AddComponent("inventory")

    inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"meat", "meat"})

    inst:AddComponent("knownlocations")
	
	inst:ListenForEvent("attacked", OnAttacked)

    MakeMediumFreezableCharacter(inst, "mowlth_bod")
	MakeMediumBurnableCharacter(inst, "mowlth_bod")
	
    MakeHauntablePanic(inst)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGmowlth")

    return inst
end

return Prefab("grotto_mowlth", fn, assets, prefabs)