local brain = require "brains/pufferbrain"

local assets =
{
    Asset("ANIM", "anim/puffer.zip"),
}

local prefabs = {}

local TARGET_MUST_TAGS = { "_combat", "character" }
local TARGET_CANT_TAGS = { "otter", "INLIMBO" }
local function FindTarget(inst, radius)
    if not inst.no_targeting then
        return FindEntity(
            inst,
            SpringCombatMod(radius),
            function(guy)
                return (not guy:HasTag("monster") or guy:HasTag("player"))
                    and inst.components.combat:CanTarget(guy)
                    and not (inst.components.follower ~= nil and inst.components.follower.leader == guy)
                    and not (inst.components.follower.leader ~= nil and inst.components.follower.leader:HasTag("player") 
                        and guy:HasTag("player") and not TheNet:GetPVPEnabled())
            end,
            TARGET_MUST_TAGS,
            TARGET_CANT_TAGS
        )
    end
end

local function Retarget(inst)
    local dist = inst.components.knownlocations:GetLocation("investigate") ~= nil and TUNING.SPIDER_INVESTIGATETARGET_DIST
        or TUNING.PUFFER_TARGET_DIST

    return FindTarget(inst, dist)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 100, .75)

    inst.DynamicShadow:SetSize(2.5, 1)
    inst.Transform:SetSixFaced()

    inst:AddTag("animal")
    inst:AddTag("otter")
    
    inst.AnimState:SetBank("puffer")
    inst.AnimState:SetBuild("puffer")
    inst.AnimState:PlayAnimation("idle", true)

    MakeInventoryFloatable(inst, "med", 0, {1.1, 0.9, 1.1})
    inst:DoTaskInTime(0, function(inst)
        inst.components.floater:OnLandedServer()
    end)
    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("timer")

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(TUNING.PUFFER_DAMAGE)
    inst.components.combat:SetAreaDamage(TUNING.DEERCLOPS_AOE_RANGE, TUNING.DEERCLOPS_AOE_SCALE)
    inst.components.combat:SetAttackPeriod(TUNING.PUFFER_ATTACK_PERIOD)
    inst.components.combat:SetRange(TUNING.PUFFER_HIT_RANGE)
    inst.components.combat:SetRetargetFunction(2, Retarget)

    inst:AddComponent("follower")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.PUFFER_HEALTH)

    inst:AddComponent("lootdropper")

    inst:AddComponent("inspectable")

    inst:AddComponent("knownlocations")

    MakeHauntablePanic(inst)

    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = TUNING.PUFFER_WALK_SPEED
    inst.components.locomotor.runspeed = TUNING.PUFFER_RUN_SPEED
	inst.components.locomotor.pathcaps = { allowocean = true, ignoreLand = true }

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetResistance(3)

    inst:SetBrain(brain)
    inst:SetStateGraph("SGpuffer")

    return inst
end

return Prefab("puffer", fn, assets, prefabs)
