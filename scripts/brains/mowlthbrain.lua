require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/wander"
require "behaviours/doaction"
require "behaviours/attackwall"
require "behaviours/panic"
require "behaviours/leash"
require "behaviours/minperiod"
require "behaviours/standstill"

local BrainCommon = require("brains/braincommon")

local MowlthBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function MowlthBrain:OnStart()
    local root = PriorityNode(
    {
        WhileNode(function() return self.inst.components.hauntable and self.inst.components.hauntable.panic end, "PanicHaunted", Panic(self.inst)),
        WhileNode(function() return self.inst.components.combat:HasTarget() end, "Flee",
            PriorityNode{
                RunAway(self.inst, {fn=function(guy) return self.inst.components.combat:TargetIs(guy) end, tags={"player"}}, 10, 10),
            }),
        WhileNode(function() return self.inst.components.health.takingfiredamage end, "OnFire", Panic(self.inst)),
        Wander(self.inst),

        StandStill(self.inst),
    },.25)

    self.bt = BT(self.inst, root)
end

return MowlthBrain