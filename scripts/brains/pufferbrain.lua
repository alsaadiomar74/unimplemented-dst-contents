require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/wander"
require "behaviours/doaction"
require "behaviours/avoidlight"
require "behaviours/panic"
require "behaviours/attackwall"
require "behaviours/useshield"

local BrainCommon = require "brains/braincommon"

local OtterBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local TRADE_DIST = 20
local GETTRADER_MUST_TAGS = { "player" }
local function GetTraderFn(inst)
    return inst.components.trader ~= nil
        and FindEntity(inst, TRADE_DIST, function(target)
                return inst.components.trader:IsTryingToTradeWithMe(target)
            end, GETTRADER_MUST_TAGS)
        or nil
end

local function KeepTraderFn(inst, target)
    return inst.components.trader ~= nil
        and inst.components.trader:IsTryingToTradeWithMe(target)
end

local function GoHomeAction(inst)
    local home = (inst.components.homeseeker ~= nil and inst.components.homeseeker.home)
        or nil
    return (home ~= nil and home:IsValid())
        and home.components.childspawner ~= nil
        and (home.components.health == nil or not home.components.health:IsDead())
        and (home.components.burnable == nil or not home.components.burnable:IsBurning())
        and BufferedAction(inst, home, ACTIONS.GOHOME)
        or nil
end

local function InvestigateAction(inst)
    if inst.components.knownlocations ~= nil then
        local investigate_pos = inst.components.knownlocations:GetLocation("investigate")
        if investigate_pos ~= nil then
            return BufferedAction(inst, nil, ACTIONS.INVESTIGATE, nil, investigate_pos, nil, 1)
        end
    end

    return nil
end

local function GetFaceTargetFn(inst)
    return inst.components.follower.leader
end

local function KeepFaceTargetFn(inst, target)
    return inst.components.follower.leader == target
end

local MAX_CHASE_TIME = 8
local DEF_MIN_FOLLOW_DIST = 2
local DEF_TARGET_FOLLOW_DIST = 5
local DEF_MAX_FOLLOW_DIST = 8
local AGG_MIN_FOLLOW_DIST = 2
local AGG_TARGET_FOLLOW_DIST = 6
local AGG_MAX_FOLLOW_DIST = 10
local MAX_WANDER_DIST = 32
function OtterBrain:OnStart()
    local root =
        PriorityNode(
        {
            BrainCommon.PanicWhenScared(self.inst, .3),
            WhileNode(function()
                        return self.inst.components.hauntable and self.inst.components.hauntable.panic
                    end, "PanicHaunted",
                Panic(self.inst)
            ),
            WhileNode(function()
                        return self.inst.components.health.takingfiredamage
                    end, "OnFire",
                Panic(self.inst)
            ),
            AttackWall(self.inst),
            ChaseAndAttack(self.inst, SpringCombatMod(MAX_CHASE_TIME)),

            IfNode(function() return self.inst.components.follower.leader ~= nil end, "HasLeader",
                FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn)
            ),

            DoAction(self.inst, function()
                return InvestigateAction(self.inst)
            end),

            FaceEntity(self.inst, GetTraderFn, KeepTraderFn),

            Wander(self.inst, function()
                    local kl = self.inst.components.knownlocations
                    return (kl ~= nil and kl:GetLocation("home")) or nil
                end,
                MAX_WANDER_DIST
            )
        }, 1.0)

    self.bt = BT(self.inst, root)
end

function OtterBrain:OnInitializationComplete()
    self.inst.components.knownlocations:RememberLocation("home", Point(self.inst.Transform:GetWorldPosition()))
end

return OtterBrain
