require("stategraphs/commonstates")

local function findtargetcheck(target)
	local x, y, z = target.Transform:GetWorldPosition()
	return TheWorld.Map:IsOceanAtPoint(x, y, z, target:HasTag("boat"))
end

local FINDEDIBLE_CANT_TAGS = { "INLIMBO", "fire", "smolder" }
local FINDEDIBLE_ONEOF_TAGS = { "boat", "edible_WOOD" }
local function CheckForBoats(inst)
    inst.target_wood = FindEntity(inst, TUNING.PUFFER_BOAT_TARGET_DIST, findtargetcheck, nil, FINDEDIBLE_CANT_TAGS, FINDEDIBLE_ONEOF_TAGS)

    if inst.target_wood then
        local x, y, z = inst.target_wood.Transform:GetWorldPosition()
        inst.target_wood:PushEvent("spawnnewboatleak", {pt = Vector3(x+math.random(-2,2), y, z+math.random(-2,2)), leak_size = "small_leak", playsoundfx = true})
    end
end

local events=
{
    CommonHandlers.OnStep(),
    CommonHandlers.OnLocomote(true, true),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnHop(),
    EventHandler("attacked", function(inst)
        if not (inst.components.health:IsDead() or inst.sg:HasStateTag("dissipate")) then
            inst.sg:GoToState("hit")
        end
    end),
    EventHandler("doattack", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            inst.sg:GoToState("attack")
        end
    end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
}

local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst, pushanim)            
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
            inst.sg:SetTimeout(1 + 2*math.random())            
            inst:PushEvent("on_landed")
        end,

        ontimeout = function(inst)            
        end,
    },

    State{
        name = "attack",
        tags = {"attack"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/attack")
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("attack")
            inst:PushEvent("on_landed")
        end,


        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) inst.components.combat:DoAttack() inst.target_wood = nil CheckForBoats(inst) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
            inst:PushEvent("on_landed")
        end,

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/death")
            inst.AnimState:PlayAnimation("death")
            inst.components.locomotor:StopMoving()
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
            inst:PushEvent("on_no_longer_landed")
        end,
    },
 }
 CommonStates.AddWalkStates(
    states,
    {
        walktimeline =
        {
            TimeEvent(1*FRAMES, function(inst) inst.didalertnoise = nil end),
            TimeEvent(15*FRAMES, function(inst) inst.SoundEmitter:PlaySound("") end),
            TimeEvent(40*FRAMES, function(inst) inst.SoundEmitter:PlaySound("") end),
        }
    })

CommonStates.AddRunStates(
    states,
    {
        starttimeline = {
            TimeEvent(10*FRAMES, function(inst)
                inst:PushEvent("on_no_longer_landed")
            end),
        },
        runtimeline =
        {
            TimeEvent(0*FRAMES, function(inst)
                SpawnAt("splash_sink", inst)
            end),
            TimeEvent(20*FRAMES, function(inst)
                SpawnAt("splash_sink", inst)
            end),
        },
        endtimeline = {
            TimeEvent(0*FRAMES, function(inst)
                inst:PushEvent("on_landed")
            end),
        },
    },
    {
        startrun = "hit",
        run = "bounce",
        stoprun = "hit_splash"
    })

CommonStates.AddSleepStates(states,
{
    starttimeline =
    {
        TimeEvent(24*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out")
        end),
    },

    sleeptimeline =
    {
        TimeEvent(4*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_in") 
        end),
        TimeEvent(49*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out") 
        end),
    },
})
CommonStates.AddFrozenStates(states)

return StateGraph("puffer", states, events, "idle")

