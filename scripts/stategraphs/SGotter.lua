require("stategraphs/commonstates")

local actionhandlers =
{
    ActionHandler(ACTIONS.EAT, "eat"),
}

local events=
{
    CommonHandlers.OnStep(),
    CommonHandlers.OnLocomote(true),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
    CommonHandlers.OnHop(),
    EventHandler("attacked", function(inst)
        if not (inst.components.health:IsDead() or inst.sg:HasStateTag("dissipate")) then
            inst.sg:GoToState("hit")
        end
    end),
    EventHandler("doattack", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or inst.components.health:IsDead()) then
            inst.sg:GoToState("attack")
        end
    end),
    EventHandler("death", function(inst) inst.sg:GoToState("death") end),
}

local states=
{
    State{
        name = "idle",
        tags = {"idle", "canrotate"},

        onenter = function(inst, pushanim)            
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("idle", true)
            inst.sg:SetTimeout(1 + 2*math.random())            
        end,

        ontimeout = function(inst)            
        end,
    },

    State{
        name = "attack",
        tags = {"attack"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/attack")
            inst.components.combat:StartAttack()
            inst.components.locomotor:StopMoving()
            if math.random() < 0.5 then
                inst.AnimState:PlayAnimation("attack")
            else
                inst.AnimState:PlayAnimation("bite")
            end
        end,


        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) inst.components.combat:DoAttack() end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "hit",
        tags = { "hit", "busy" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "taunt",
        tags = {"busy"},

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("taunt")
        end,

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "eat",
        tags = {"busy"},

        onenter = function(inst, forced)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pre")
            inst.sg.statemem.forced = forced
        end,

        events =
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("eat_pst")
            end),
        },
    },

    State{
        name = "eat_pst",
        tags = {"busy"},

        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_pst", false)
        end,

        timeline =
        {
            TimeEvent(30 * FRAMES, function(inst)
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animover", function(inst)  inst.SoundEmitter:KillSound("eating") inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/death")
            inst.AnimState:PlayAnimation("death")
            inst.components.locomotor:StopMoving()
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
        end,
    },
    State{
        name = "hop_pre",
        tags = { "doing", "busy", "jumping", "canrotate" },

        onenter = function(inst)
			inst.sg.statemem.swimming = inst:HasTag("swimming")
            if inst.sg.statemem.swimming then
                inst.AnimState:PlayAnimation("jumpout")
            else
                inst.AnimState:PlayAnimation("jump")
            end
			if not inst.sg.statemem.swimming then
				inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
			end
			if inst.components.embarker:HasDestination() then
	            inst.sg:SetTimeout(18 * FRAMES)
                inst.components.embarker:StartMoving()
			else
	            inst.sg:SetTimeout(18 * FRAMES)
                if inst.landspeed then
                    inst.components.locomotor.runspeed = inst.landspeed
                end
                inst.components.locomotor:RunForward()
			end

			--if onenters ~= nil and onenters.hop_pre ~= nil then
			--	onenters.hop_pre(inst)
			--end
        end,

	    onupdate = function(inst,dt)
			if inst.components.embarker:HasDestination() then
				if inst.sg.statemem.embarked then
					inst.components.embarker:Embark()
					inst.components.locomotor:FinishHopping()
					inst.sg:GoToState("hop_pst", false)
				elseif inst.sg.statemem.timeout then
					inst.components.embarker:Cancel()
					inst.components.locomotor:FinishHopping()

					local x, y, z = inst.Transform:GetWorldPosition()
					inst.sg:GoToState("hop_pst", not TheWorld.Map:IsVisualGroundAtPoint(x, y, z) and inst:GetCurrentPlatform() == nil)
				end
            elseif inst.sg.statemem.timeout or
                   (inst.sg.statemem.tryexit and inst.sg.statemem.swimming == TheWorld.Map:IsVisualGroundAtPoint(inst.Transform:GetWorldPosition())) or
                   (not inst.components.locomotor.dest and not inst.components.locomotor.wantstomoveforward) then
				inst.components.embarker:Cancel()
				inst.components.locomotor:FinishHopping()
				local x, y, z = inst.Transform:GetWorldPosition()
				inst.sg:GoToState("hop_pst", not TheWorld.Map:IsVisualGroundAtPoint(x, y, z) and inst:GetCurrentPlatform() == nil)
			end
		end,

        timeline = {
            TimeEvent(0, function(inst)
                if inst:HasTag("swimming") then
                    SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
                end
            end),
            TimeEvent(9 * FRAMES, function(inst)
                if inst.sg.statemem.swimming then
                    inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
                end
            end),
        },

		ontimeout = function(inst)
			inst.sg.statemem.timeout = true
		end,

        events =
        {
            EventHandler("done_embark_movement", function(inst)
				if not inst.AnimState:IsCurrentAnimation("jump_loop") then
					inst.AnimState:PlayAnimation("jump_loop", false)
				end
				inst.sg.statemem.embarked = true
            end),
            EventHandler("animover", function(inst)
				if not inst.AnimState:IsCurrentAnimation("jump_loop") then
					if inst.AnimState:AnimDone() then
						if not inst.components.embarker:HasDestination() then
							inst.sg.statemem.tryexit = true
						end
					end
					inst.AnimState:PlayAnimation("jump_loop", false)
				end
            end),
        },

		onexit = function(inst)
            inst.Physics:CollidesWith(COLLISION.LIMITS)
			if inst.components.embarker:HasDestination() then
				inst.components.embarker:Cancel()
				inst.components.locomotor:FinishHopping()
			end

			--if onexits ~= nil and onexits.hop_pre ~= nil then
			--	onexits.hop_pre(inst)
			--end
		end,
    },

    State{
        name = "hop_pst",
        tags = { "busy", "jumping" },

        onenter = function(inst, land_in_water)
			if land_in_water then
				inst.components.amphibiouscreature:OnEnterOcean()
                inst.AnimState:SetBank("otter_basics_water")
                inst.AnimState:PlayAnimation("jumpin_pst")
			else
				inst.components.amphibiouscreature:OnExitOcean()
                inst.AnimState:SetBank("otter_basics")
                inst.AnimState:PlayAnimation("jump_pst")
			end

			--if onenters ~= nil and onenters.hop_pst ~= nil then
			--	onenters.hop_pst(inst)
			--end
        end,

       timeline = {
            TimeEvent(4 * FRAMES, function(inst)
                if inst:HasTag("swimming") then
                    inst.components.locomotor:Stop()
                    SpawnPrefab("splash_green").Transform:SetPosition(inst.Transform:GetWorldPosition())
                end
            end),
            TimeEvent(6 * FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    inst.components.locomotor:StopMoving()
                end
            end),
        },

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },

        onexit = function(inst)
			--if onexits ~= nil and onexits.hop_pst ~= nil then
			--	onexits.hop_pst(inst)
			--end
		end,
    },

    State{
        name = "hop_antic",
        tags = { "doing", "busy", "jumping", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
            inst.sg.statemem.swimming = inst:HasTag("swimming")

            inst.AnimState:PlayAnimation("jump_antic")

            inst.sg:SetTimeout(30 * FRAMES)

			--if onenters ~= nil and onenters.hop_antic ~= nil then
			--	onenters.hop_antic(inst)
			--end
        end,

        --timeline = timelines.hop_antic,

        ontimeout = function(inst)
            inst.sg:GoToState("hop_pre")
        end,
        onexit = function(inst)
			--if onexits ~= nil and onexits.hop_antic ~= nil then
			--	onexits.hop_antic(inst)
			--end
        end,
    },
 }

CommonStates.AddRunStates(
    states,
    {
        runtimeline =
        {
        TimeEvent(0, function(inst)
            if inst:HasTag("swimming") then
                inst.SoundEmitter:PlaySound("turnoftides/common/together/water/swim/run_water_med")
            else   
            end
        end),
        TimeEvent(1*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(2*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(7*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        TimeEvent(8*FRAMES, function(inst)
                if not inst:HasTag("swimming") then
                    PlayFootstep(inst)
                end
        end),
        }
    })

CommonStates.AddSleepStates(states,
{
    starttimeline =
    {
        TimeEvent(24*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out")
        end),
    },

    sleeptimeline =
    {
        TimeEvent(4*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_in") 
        end),
        TimeEvent(49*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound("waterlogged2/creatures/grass_gator/sleep_out") 
        end),
    },
})
CommonStates.AddFrozenStates(states)

return StateGraph("otter", states, events, "idle", actionhandlers)

