name = "1. Unimplemented Contents"
description = "holy crap, moth thing."
author = "Hornet, Asura"
version = "1.0"

forumthread = ""

api_version = 10

dst_compatible = true
forge_compatible = true
gorge_compatible = true

dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false

all_clients_require_mod = true 

icon_atlas = "modicon.xml"
icon = "modicon.tex"

server_filter_tags = {
	"unused", "unimplemented", 
}