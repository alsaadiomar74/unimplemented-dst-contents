env._G = GLOBAL
env.require = _G.require

local ToLoad = require("main/to_load")

PrefabFiles = ToLoad.Prefabs
Assets = ToLoad.Assets

require("main/strings")
require("main/tuning")
modimport("scripts/main/main")
